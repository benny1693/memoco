/**
 * @file TSPProblem.hpp
 * @author Benedetto Cosentino (benedetto.cosentino@studenti.unipd.it)
 * @brief Header file for the TSPProblem
 * @version 1
 * @date 2020-12-31
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef TSPPROBLEM_H
#define TSPPROBLEM_H

#include <cmath>
#include <utility>
#include <vector>

using std::vector;

typedef unsigned int u_int;

typedef std::pair<u_int, u_int> Position;

/**
 * @brief   Class that models an instance of the Travelling Salesman Problem
 *
 */
class TSPProblem {
private:
  vector<vector<double>> costs;

public:
  /**
   * @brief   Construct a new TSPProblem object using the hole positions and
   *          calculating the costs for each arc
   *
   * @param   holePositions:  a vector of Position(s) where the two member
   *                          indicate the horizontal and vertical
   *                          (respectively) positioning
   */
  TSPProblem(const vector<Position> &holePositions);

  /**
   * @brief Returns the cost between two nodes
   *
   * @param i Index of the first node
   * @param j Index of the second node
   * @return double
   */
  double c(u_int i, u_int j) const;

  /**
   * @brief Returns the number of nodes of the problem
   *
   * @return u_int
   */
  u_int n() const;
};

#endif