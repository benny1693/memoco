/**
 * @file CPLEXSolver.hpp
 * @author Benedetto Cosentino (benedetto.cosentino@studenti.unipd.it)
 * @brief Header file for CPLEXSolver
 * @version 1
 * @date 2020-12-31
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef CPLEXSOLVER_H
#define CPLEXSOLVER_H

#include <chrono>
#include <iostream>
#include <string>
#include <vector>

#include "TSPProblem.hpp"
#include "TSPSolution.hpp"
#include "cpxmacro.h"

using std::string;
using std::vector;

typedef std::chrono::steady_clock::duration Duration;
typedef std::chrono::steady_clock::time_point Time;

/**
 * @brief   Class that, given an instance of TSP, solve the problem using
 *          a linear programming model
 */
class CPLEXSolver {
private:
  static const int BUF_SIZE = 512;

  // CPLEX enviroment and problem
  Env env;
  Prob lp;

  // Error status variables
  int status;
  char errmsg[BUF_SIZE];

  // Indexes of the variables (linearized) --> A[i][j] = A[i*N + j]
  vector<u_int> X;
  vector<u_int> Y;

  // Dimension of the problem
  u_int N;

  // Elapsed time of the computation
  Duration time_el;

  /**
   * @brief Access to the index of variable x_i,j of the original problem
   *
   * @param i     Row position in the original problem
   * @param j     Column position in the original problem
   * @return u_int&
   */
  u_int &x(u_int i, u_int j);
  u_int &y(u_int i, u_int j);

  /**
   * @brief Find the path of the solution found by CPLEX
   *
   * @return vector<u_int>    The sequence of node that describes the
   *                          solution path
   */
  vector<u_int> generate_path();

public:
  /**
   * @brief   Construct a new CPLEXSolver object given an instance of the
   *          Travelling Salesman Problem
   *
   * @param problem   Instance of TSP
   */
  CPLEXSolver(const TSPProblem &problem);

  /**
   * @brief Destroy the CPLEXSolver object
   *
   */
  ~CPLEXSolver();

  /**
   * @brief Set the time limit for the object
   *
   * @param time_limit    A time limit (seconds)
   */
  void setTimeLimit(double time_limit);

  /**
   * @brief   Solve the problem using the CPLEX library
   *
   */
  void solve();

  /**
   * @brief Generate and returns the solution of the problem
   *
   * @return TSPSolution
   */
  TSPSolution generate_solution();

  /**
   * @brief Return the time elapsed (in microseconds) during the
   *        computation done by CPLEX.
   *        Recall: 1 microseconds = 1e-6 seconds
   *
   * @return double
   */
  double time_elapsed() const;

  /**
   * @brief   Prints in a file with a given name the linear programming
   *          model for the instance of TSP
   *
   * @param namefile  Name of the file
   */
  void printModel(string namefile);

  /**
   * @brief   Prints in a XML file with a given name solution of
   *          the linear programming model for the instance of TSP
   *
   * @param namefile  Name of the file
   */
  void printSolution(string namefile);
};
#endif