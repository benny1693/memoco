/**
 * @file LocalSearch.hpp
 * @author Benedetto Cosentino (benedetto.cosentino@studenti.unipd.it)
 * @brief Header file for LocalSearch class
 * @version 1
 * @date 2021-01-05
 *
 * @copyright Copyright (c) 2021
 *
 */
#ifndef LOCALSEARCH_H
#define LOCALSEARCH_H

#include "Successor.hpp"
#include "TSPProblem.hpp"
#include "TSPSolution.hpp"

#include <algorithm>
#include <ctime>
#include <limits>
#include <memory>
#include <queue>
#include <random>

/**
 * @brief Class that solves a Travelling Salesman Problem with a Local Search
 *        approach.
 */
class LocalSearch {
private:
  /**
   * @brief Returns a randomly generated solution
   *
   * @param seed  seed for the random generation
   * @return TSPSolution
   */
  TSPSolution random_solution() const;

  /**
   * @brief Solve the TSP using a random solution as start solution
   *
   * @param seed seed of the randomly generated start solution
   * @return TSPSolution
   */
  TSPSolution random_solve() const;

protected:
  const TSPProblem &problem;

  virtual std::unique_ptr<Successor>
  next_move(const TSPSolution &solution) const = 0;

public:
  /**
   * @brief Construct a new Local Search object
   *
   * @param problem   the problem to solve
   */
  LocalSearch(const TSPProblem &problem);

  /**
   * @brief Destroy the Local Search object
   */
  virtual ~LocalSearch() = default;

  /**
   * @brief Solve the TSP starting from a given solution
   *
   * @param start_solution    the solution from which starts the computation
   * @return TSPSolution
   */
  TSPSolution solve(const TSPSolution &start_solution) const;

  /**
   * @brief Solve the TSP using a given amount of random solution
   *
   * @param n the amount of random solution to generate
   * @return TSPSolution
   */
  TSPSolution random_multistart_solve(u_int n) const;
};

#endif // LOCALSEARCH_H