/**
 * @file Parser.hpp
 * @author Benedetto Cosentino (benedetto.cosentino@studenti.unipd.it)
 * @brief Header file for the Parser
 * @version 1
 * @date 2020-12-31
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef PARSER_H
#define PARSER_H

#include <fstream>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>

#include "TSPProblem.hpp"

/**
 * @brief   Class that parse a file that contains a list of coordinates (x,y)
 *          in a graph.
 *          The file must be in the following format:
 *          BEGIN
 *          X1  Y1
 *          X2  Y2
 *          .   .
 *          .   .
 *          .   .
 *          XN  YN
 *          END
 */
class Parser {
public:
  /**
   * @brief   Read a file with a given filename and returns an instance of the
   *          Travelling Salesman Problem
   *
   * @param filename      Name of the file to parse
   * @param print         true iff the function prints in the standard output
   *                      the position
   * @return TSPProblem
   */
  TSPProblem readFile(const std::string &filename, bool print = false) const;
};

#endif