/**
 * @file Neighbourhood.hpp
 * @author Benedetto Cosentino (benedetto.cosentino@studenti.unipd.it)
 * @brief Header file for Neighbourhood interface
 * @version 1
 * @date 2021-01-03
 *
 * @copyright Copyright (c) 2021
 *
 */

#ifndef NEIGHBOURHOOD_H
#define NEIGHBOURHOOD_H

#include "Successor.hpp"
#include "TSPProblem.hpp"
#include "TSPSolution.hpp"

#include <memory>

/**
 * @brief Class that models the choice of the next solution to chose among the
 *        ones in the neighbourhood
 */
class Neighbourhood {
protected:
  const TSPProblem &problem;
  const TSPSolution &solution;

public:
  /**
   * @brief Construct a new Neighbourhood object
   *
   * @param problem   the instance of TSP
   * @param solution  the solution given for the neighbourhood computation
   */
  Neighbourhood(const TSPProblem &problem, const TSPSolution &solution);

  /**
   * @brief Destroy the Neighbourhood object
   */
  virtual ~Neighbourhood() = default;

  /**
   * @brief Finds the next move to apply to the solution
   *
   * @return std::unique_ptr<Successor>
   */
  virtual std::unique_ptr<Successor> findMove() const = 0;
};

#endif // NEIGHBOURHOOD_H