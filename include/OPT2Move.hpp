/**
 * @file OPT2Move.hpp
 * @author Benedetto Cosentino (benedetto.cosentino@studenti.unipd.it)
 * @brief Header file for class OPT2Move
 * @version 1
 * @date 2021-01-02
 *
 * @copyright Copyright (c) 2021
 *
 */

#ifndef OPT2MOVE_H
#define OPT2MOVE_H

#include <stdexcept>

#include "Successor.hpp"

/**
 * @brief Class that models a move for 2-OPT neighbourhood function
 */
class OPT2Move : public Successor {
private:
  u_int from, to;

public:
  /**
   * @brief Construct a new OPT2Move object using two indexes
   *
   * @param i    first index
   * @param j    second index
   */
  OPT2Move(u_int i, u_int j);

  /**
   * @brief Applies the 2-OPT move to a given solution
   *
   * @param problem   The problem of the given solution
   * @param solution  The solution to which the move must be applied
   */
  virtual void apply(const TSPProblem &problem,
                     TSPSolution &solution) const override;

  /**
   * @brief Returns the variation of the cost of the given solution after
   *        the application of the move
   *
   * @param problem   The problem that contains the costs
   * @param solution  The solution to which the move must be applied
   * @return double
   */
  virtual double variation(const TSPProblem &problem,
                           const TSPSolution &solution) const override;
};

#endif // OPT2MOVE_H