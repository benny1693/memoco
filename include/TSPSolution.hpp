/**
 * @file TSPSolution.hpp
 * @author Benedetto Cosentino (benedetto.cosentino@studenti.unipd.it)
 * @brief Header file for TSPSolution
 * @version 1
 * @date 2020-12-31
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef TSPSOLUTION_H
#define TSPSOLUTION_H

#include <iostream>
#include <vector>

using std::vector;

typedef unsigned int u_int;

/**
 * @brief   Class that models the solution of an instance of the
 *          Travelling Salesman Problem
 *
 */
class TSPSolution {
public:
  /**
   * @brief Vector that contains the sequence of nodes of the solution
   */
  vector<u_int> nodes;

  /**
   * @brief Variable that contains the value of the solution
   */
  double cost;
};

/**
 * @brief Output operator
 *
 * @param out   Output stream
 * @param sol   Solution to print in the stream
 * @return std::ostream&
 */
std::ostream &operator<<(std::ostream &out, const TSPSolution &sol);

#endif