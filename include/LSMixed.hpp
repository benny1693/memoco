/**
 * @file LSMixed.hpp
 * @author Benedetto Cosentino (benedetto.cosentino@studenti.unipd.it)
 * @brief  Header file for LSMixed class
 * @version 1
 * @date 2021-01-05
 *
 * @copyright Copyright (c) 2021
 *
 */
#ifndef LSMIXED_H
#define LSMIXED_H

#include "LocalSearch.hpp"
#include "OPTMixed.hpp"

/**
 * @brief Class that solves a Travelling Salesman Problem with a Local Search
 *        approach.
 *        The neighbourhood function is determined by a value R in [0,1].
 *        If R = 0, then the function is 3-OPT.
 *        If R = 1, then the function is 2-OPT.
 *        In any other cases, if the rate
 *                      (cost + variation)/cost
 *        calculated on an intermediate solution is greater than R the object
 *        use a 3-OPT approach, otherwise a 2-OPT approach.
 */
class LSMixed : public LocalSearch {
private:
  double rate;

protected:
  std::unique_ptr<Successor>
  next_move(const TSPSolution &solution) const override;

public:
  /**
   * @brief Construct a new Local Search object
   *
   * @param problem   the problem to solve
   * @param rate      the rate that triggers the 3-OPT search
   */
  LSMixed(const TSPProblem &problem, double rate);

  /**
   * @brief Destroy the Local Search object
   */
  virtual ~LSMixed() = default;
};

#endif // LSMIXED_H