/**
 * @file OPT3Move.hpp
 * @author Benedetto Cosentino (benedetto.cosentino@studenti.unipd.it)
 * @brief Header file for class OPT3Move
 * @version 1
 * @date 2021-01-02
 *
 * @copyright Copyright (c) 2021
 *
 */

#ifndef OPT3MOVE_H
#define OPT3MOVE_H

#include <stdexcept>

#include "OPT2Move.hpp"
#include "Successor.hpp"

/**
 * @brief Class that models a move for 3-OPT neighbourhood function. 3-OPT
 *        provides 4 neighbours: this class applies the best neighbour among
 *        them.
 *        Considering a move (i,j,k) and a solution in this form:
 *                             ...hi...jm...kl...
 *        The 4 possible neighbours are:
 *        1) ...hj...ik...ml...
 *        2) ...hm...ki...jl...
 *        3) ...hk...mi...jl...
 *        4) ...hm...kj...il...
 *
 *        Anyway, if two of the arcs chosen are contiguous, the only possible
 *        neghbour is the one described by case (2)
 */
class OPT3Move : public Successor {
private:
  u_int from, through, to;

  /**
   * @brief Returns the variations of the 4 possible neighbours provided by
   *        3-OPT.
   *
   * @param problem   The TSP instance
   * @param solution  The TSP solution to improve
   * @return vector<double>
   */
  vector<double> vars(const TSPProblem &problem,
                      const TSPSolution &solution) const;

public:
  /**
   * @brief Construct a new OPT3Move object using three indexes
   *
   * @param i    first index
   * @param j    second index
   * @param k    third index
   */
  OPT3Move(u_int i, u_int j, u_int k);

  /**
   * @brief Applies the 3-OPT move to a given solution.
   *        Recall: the method chooses the best neighbour among the 4
   *                possible candidates
   *
   * @param problem   The problem of the given solution
   * @param solution  The solution to which the move must be applied
   */
  virtual void apply(const TSPProblem &problem,
                     TSPSolution &solution) const override;

  /**
   * @brief Returns the variation of the cost of the given solution after
   *        the application of the move
   *
   * @param problem   The problem that contains the costs
   * @param solution  The solution to which the move must be applied
   * @return double
   */
  virtual double variation(const TSPProblem &problem,
                           const TSPSolution &solution) const override;
};

#endif // OPT3MOVE_H