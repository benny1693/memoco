/**
 * @file instances_generator.hpp
 * @author Benedetto Cosentino (benedetto.cosentino@studenti.unipd.it)
 * @brief  Header file for InstancesGenerator
 * @version 1
 * @date 2020-12-31
 *
 * @copyright Copyright (c) 2020
 *
 */
#ifndef INSTANCES_GENERATOR_H
#define INSTANCES_GENERATOR_H

#include <ctime>
#include <fstream>
#include <iostream>
#include <string>
#include <unordered_set>
#include <vector>

/**
 * @brief Class that generates the random graphs G = (V,A) with a given
 * dimension N , a given limit for the coordinates and for the number of
 * nodes for each value of the coordinate x
 */
class InstancesGenerator {
private:
  // Name
  std::string instances_name;
  std::string dir_name;

  // |V|
  u_int n_nodes;

  // Limits
  u_int limit_x, limit_y, maximum_holes;

public:
  /**
   * @brief Construct a new Instances Generator object in the same directory
   *        of the executable
   *
   * @param name                  Name of the instance
   * @param N                     |V|
   * @param max_x                 Limit for coordinate x
   *                              (for each x: 0 <= x <= max_x)
   * @param max_y                 Limit for coordinate y
   *                              (for each y: 0 <= y <= max_y)
   * @param max_holes_per_row     Limit for the number of nodes
   *                              for each value of the coordinate x
   */
  InstancesGenerator(std::string name, u_int N, u_int max_x = 1000,
                     u_int max_y = 1000, u_int max_holes_per_row = 10);

  /**
   * @brief Construct a new Instances Generator object in a given directory
   *
   * @param name                  Name of the instance
   * @param dname                 Name of the destination directory
   * @param N                     |V|
   * @param max_x                 Limit for coordinate x
   *                              (for each x: 0 <= x <= max_x)
   * @param max_y                 Limit for coordinate y
   *                              (for each y: 0 <= y <= max_y)
   * @param max_holes_per_row     Limit for the number of nodes
   *                              for each value of the coordinate x
   */
  InstancesGenerator(std::string name, std::string dname, u_int N,
                     u_int max_x = 1000, u_int max_y = 1000,
                     u_int max_holes_per_row = 10);

  /**
   * @brief Generate a single instance with the parameters given during
   *        the construction of the object
   *
   * @param seed  Seed used to randomize generation if too fast
   */
  void generate_single_instance(u_int seed = 0);

  /**
   * @brief Generate multiple instances with the parameters given during
   *        the construction of the object
   *
   * @param n_instances   Number of instances to generate
   */
  void generate_multiple_instances(u_int n_instances);
};

#endif