/**
 * @file utilities.hpp
 * @author Benedetto Cosentino (benedetto.cosentino@studenti.unipd.it)
 * @brief File that contains utility functions for the various scripts in the
 *        project
 * @version 1
 * @date 2021-01-06
 *
 * @copyright Copyright (c) 2021
 *
 */

#ifndef UTILITIES_H
#define UTILITIES_H

#include <dirent.h>
#include <sys/stat.h>

#include <chrono>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

using std::string;
using std::vector;

namespace utils {
  typedef std::chrono::steady_clock::time_point Time;

  vector<string> listFiles(string dir_name);

  template <typename T> T to_(const char *arg) {
    std::stringstream ss(arg);
    T x;
    if (!(ss >> x))
      throw std::runtime_error("Invalid number: " + std::string(arg));
    else if (!ss.eof())
      throw std::runtime_error("Trailing characters after number: " +
                               std::string(arg));

    return x;
  }
} // namespace utils

#endif