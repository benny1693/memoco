/**
 * @file OPT2.hpp
 * @author Benedetto Cosentino (benedetto.cosentino@studenti.unipd.it)
 * @brief Header file for OPT2 class
 * @version 1
 * @date 2021-01-04
 *
 * @copyright Copyright (c) 2021
 *
 */

#ifndef OPT2_H
#define OPT2_H

#include "Neighbourhood.hpp"
#include "OPT2Move.hpp"

/**
 * @brief Class that models the choice of the next solution to chose among the
 *        ones in a 2-OPT neighbourhood
 */
class OPT2 : public Neighbourhood {
public:
  /**
   * @brief Construct a new OPT2 object
   *
   * @param problem   the instance of TSP
   * @param solution  the solution given for the neighbourhood computation
   */
  OPT2(const TSPProblem &problem, const TSPSolution &solution);

  /**
   * @brief Destroy the OPT2 object
   */
  virtual ~OPT2() = default;

  /**
   * @brief Finds the best improving move in the 2-OPT neighbourhood to apply
   *        to the solution
   *
   * @return std::unique_ptr<Successor>
   */
  virtual std::unique_ptr<Successor> findMove() const override;
};

#endif // OPT2_H