/**
 * @file OPT3.hpp
 * @author Benedetto Cosentino (benedetto.cosentino@studenti.unipd.it)
 * @brief Header file for OPT3 class
 * @version 1
 * @date 2021-01-04
 *
 * @copyright Copyright (c) 2021
 *
 */

#ifndef OPT3_H
#define OPT3_H

#include "Neighbourhood.hpp"
#include "OPT3Move.hpp"

/**
 * @brief Class that models the choice of the next solution to chose among the
 *        ones in a 3-OPT neighbourhood
 */
class OPT3 : public Neighbourhood {
public:
  /**
   * @brief Construct a new OPT2 object
   *
   * @param problem   the instance of TSP
   * @param solution  the solution given for the neighbourhood computation
   */
  OPT3(const TSPProblem &problem, const TSPSolution &solution);

  /**
   * @brief Destroy the OPT2 object
   */
  virtual ~OPT3() = default;

  /**
   * @brief Finds the best improving move in the 3-OPT neighbourhood to apply
   *        to the solution
   *
   * @return std::unique_ptr<Successor>
   */
  virtual std::unique_ptr<Successor> findMove() const override;
};

#endif // OPT3_H