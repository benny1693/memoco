/**
 * @file OPTMixed.hpp
 * @author Benedetto Cosentino (benedetto.cosentino@studenti.unipd.it)
 * @brief Header file for OPTMixed class
 * @version 1
 * @date 2021-01-04
 *
 * @copyright Copyright (c) 2021
 *
 */

#ifndef OPTMIXED_H
#define OPTMIXED_H

#include "Neighbourhood.hpp"
#include "OPT2.hpp"
#include "OPT3.hpp"

/**
 * @brief Class that models the choice of the next solution to chose among the
 *        ones in a 2-OPT neighbourhood and a 3-OPT neighbourhood.
 *        The objects of this class search in the solution of the 3-OPT
 *        neighbourhood only if the rate
 *                          (cost + variation)/cost
 *        is greater than to a given parameter R (default = 1).
 *        R should be always in [0,1] interval. Note that the smaller the value
 *        of R the slower the search is going to be.
 */
class OPTMixed : public Neighbourhood {
private:
  double rate;

public:
  /**
   * @brief Construct a new OPTMixed object
   *
   * @param problem   the instance of TSP
   * @param solution  the solution given for the neighbourhood computation
   * @param rate      the rate that triggers the 3-OPT search
   */
  OPTMixed(const TSPProblem &problem, const TSPSolution &solution,
           double rate = 1);

  /**
   * @brief Destroy the OPTMixed object
   */
  virtual ~OPTMixed() = default;

  /**
   * @brief Finds the best improving move in the 2-OPT and 3-OPT
   *        neighbourhood to apply to the solution if the
   *
   * @return std::unique_ptr<Successor>
   */
  virtual std::unique_ptr<Successor> findMove() const override;
};

#endif // OPTMIXED_H