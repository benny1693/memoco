/**
 * @file Successor.hpp
 * @author Benedetto Cosentino (benedetto.cosentino@studenti.unipd.it)
 * @brief Header file for class Successor
 * @version 1
 * @date 2021-01-02
 *
 * @copyright Copyright (c) 2021
 *
 */

#ifndef OPTMOVE_H
#define OPTMOVE_H

#include "TSPProblem.hpp"
#include "TSPSolution.hpp"

typedef unsigned int u_int;

/**
 * @brief Interface that models a move for k-OPT neighbourhood functions
 */
class Successor {
public:
  /**
   * @brief Destroy the Successor object
   */
  virtual ~Successor() = default;

  /**
   * @brief Replace the actual solution with its successor
   *
   * @param problem   The problem of the given solution
   * @param solution  The solution to which the move must be applied
   */
  virtual void apply(const TSPProblem &problem,
                     TSPSolution &solution) const = 0;

  /**
   * @brief Returns the variation of the cost of the given solution after
   *        the solution replace
   *
   * @param problem   The problem that contains the costs
   * @param solution  The solution to which the move must be applied
   * @return double
   */
  virtual double variation(const TSPProblem &problem,
                           const TSPSolution &solution) const = 0;
};

#endif // OPTMOVE_H