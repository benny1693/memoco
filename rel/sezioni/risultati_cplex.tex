% Risultati CPLEX ottenuti per limite di tempo (0.1s, 0.5s, 1s, 5s, 10s, 50s, 100s)
%             |            limite 0.1             |              limite 0.5           |...
%             | tempo impiegato | costo soluzione | tempo impiegato | costo soluzione |...
% istanza     |                                   |                                   |...

% Statistiche interessanti:
% - se esistono soluzioni con limite maggiore, differenza percentuale rispetto a quella ottenuta
% - frequenza di raggiungimento del limite di tempo (x = dimensione istanza, y = numero di volte in cui il limite è raggiunto)
% - andamento dei tempi di esecuzione in base alla dimensione (solo per il limite massimo (100s))

% Conclusioni:
% - quale limite conviene mettere? perché?

Per quanto riguarda i risultati ottenuti da \texttt{cplexsolver}, ho raccolto le soluzioni impostando un limite di tempo di 0.1, 0.5, 1, 5, 10, 50, 100 secondi.\\
Ho, poi, considerato la differenza percentuale della soluzione rispetto a quella ottenuta con limite di 100 secondi, che dovrebbe anche essere la migliore.\\
Altra statistica interessante è la percentuale di raggiungimento del tempo limite in base alla dimensione delle istanze.\\
Infine, ho riportato i tempi di esecuzione del programma.

\subsubsection{Differenze percentuali}
Come visto a lezione, la qualità delle soluzioni migliora man mano che il tempo limite aumenta (Figura 10): in particolare, il miglioramento è più evidente nelle prime iterazioni (tra 1 e 10 secondi). Inoltre, la precisione della soluzioni diminuisce all'aumentare della dimensione: in sostanza, più grande è l'istanza del problema più tempo sarà necessario per ottenere la soluzione ottima.\\
La Figura 11 mostra questo fatto, illustrando le differenze percentuali in base alla dimensione per ogni limite di tempo: quando esso è pari a 0.1 secondi già le istanze con $|N| \geqslant 15$ hanno soluzioni peggiori del 125\% rispetto a quella ottenuta con limite di 100 secondi e non esiste alcuna soluzione per quelle con $|N| > 30$.\\
Con la soglia di 0.5 secondi aumenta la dimensione delle istanze risolvibili fino a 35 nodi e diminuiscono le differenze percentuali (massimo di circa 25\% per le istanze con 30 nodi).\\
Quando il limite è di 1 secondo, si ha un notevole miglioramento dal punto di vista delle dimensioni delle istanze risolte (fino a 90 nodi), tuttavia le soluzioni sono peggiori di circa $3.5$ volte rispetto a quelle ottenute con un limite di 100 secondi. Invece, per le istanze con al massimo 30 nodi è possibile calcolare, nella maggior parte dei casi, una soluzione abbastanza precisa.\\
Con una soglia di 5 secondi, per quasi tutte le istanze con al massimo 40 nodi è possibile ottenere soluzioni abbastanza vicine all'ottimo, tuttavia ciò non vale per quelle con più di 60 nodi per cui, mediamente, le soluzioni sono peggiori del 75\%.\\
Con un limite di 10 secondi, le soluzioni ottenute sono abbastanza precise finché la dimensione del problema non supera i 50 nodi, altrimenti la differenza percentuale può raggiungere anche il 225\%.\\
Quando il limite raggiunge i 50 secondi, le soluzioni migliorano di molto (anche per istanze con 70 nodi si ottengono soluzioni con differenza percentuale inferiore al 20\%): in particolare, per le istanze con 60 nodi si raggiunge spesso l'ottimo.

\begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{img/diff_perc_time_limit.png}
    \caption{Differenze percentuali per tempo limite (media)}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=0.45\textwidth]{img/diff_perc_01.png}
    \includegraphics[width=0.45\textwidth]{img/diff_perc_05.png}
    \\
    \includegraphics[width=0.45\textwidth]{img/diff_perc_1.png}
    \includegraphics[width=0.45\textwidth]{img/diff_perc_5.png}
    \\
    \includegraphics[width=0.45\textwidth]{img/diff_perc_10.png}
    \includegraphics[width=0.45\textwidth]{img/diff_perc_50.png}
    \caption{Differenze percentuali per dimensione con limite 0.1, 0.5, 1, 5, 10, 50}
\end{figure}

\subsubsection{Raggiungimento del limite di tempo}
In generale, all'aumentare del limite di tempo messo a disposizione aumenta il numero e la dimensione delle istanze per cui è possibile fornire una soluzione ottima.\\
Come si può notare dai grafici in Figura 12, il numero di istanze risolvibili all'ottimo con limite 0.1 è scarso e si tratta comunque di istanze molto piccole (10 nodi): infatti, già quando i grafi possiedono 15 nodi solo poco meno del 20\% ottiene una soluzione ottima.\\
Chiaramente, la situazione migliora all'aumentare del limite di tempo: infatti entro i 0.5 secondi per tutte le istanze con 15 nodi è possibile ottenere la soluzione ottima, così come per quelle da 20 con limite 1 secondo.\\
Quando la soglia è pari a 5 secondi, tutte le istanze con dimensione pari a o minore di 30 vengono risolte prima del raggiungimento del limite. Invece, se esso è uguale a 10, quasi tutti i grafi con 40 nodi vengono risolti all'ottimo.\\
Si può notare un sensibile miglioramento quando il limite è pari a 50 secondi: infatti, per quasi tutte le istanze con dimensione minore o uguale a 60 è possibile raggiungere la soluzione ottima. Il miglioramento non è così drastico quando il limite è di 100 secondi: le istanze risolvibili all'ottimo hanno generalmente al massimo 70 nodi.

\begin{figure}
    \centering
    \includegraphics[width=0.45\textwidth]{img/freq_time_01.png}
    \includegraphics[width=0.45\textwidth]{img/freq_time_05.png}\\
    \includegraphics[width=0.45\textwidth]{img/freq_time_1.png}
    \includegraphics[width=0.45\textwidth]{img/freq_time_5.png}\\
    \includegraphics[width=0.45\textwidth]{img/freq_time_10.png}
    \includegraphics[width=0.45\textwidth]{img/freq_time_50.png}\\
    \includegraphics[width=0.45\textwidth]{img/freq_time_100.png}
    \caption{Frequenza di raggiungimento del tempo limite (0.1, 0.5, 1, 5, 10, 50, 100)}
\end{figure}

\subsubsection{Tempi di esecuzione}
Il tempo di esecuzione di \texttt{cplexsolver} diventa già inaccettabile per istanze con dimensione maggiore di 45 (circa 10 secondi). Inoltre, per grafi con 80 nodi si superano persino i 90 secondi, mentre per quelli con 85 nodi si raggiunge il limite massimo di 100 secondi (Figura 13).\\
Il grafico in Figura 14 mostra la deviazione standard del tempo di esecuzione ed evidenzia come esso vari molto dipendentemente dalla specifica istanza: infatti, le istanze più grandi (che non raggiungono il limite di 100 secondi, cioè con $N \in [60,70]$) hanno tempi di esecuzione molto variabili. Ad esempio, considerando le istanze per cui $N = 65$, il tempo di esecuzione medio è $5.246904$ secondi e, tuttavia, vi sono diverse istanze che superano i 70 secondi.

\begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{img/time_elapsed_cplex.png}
    \caption{Tempo di esecuzione di \texttt{cplexsolver} (media)}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{img/time_elapsed_cplex_std.png}
    \caption{Tempo di esecuzione di \texttt{cplexsolver} (deviazione standard)}
\end{figure}

\subsubsection{Conclusioni}
A fronte dei risultati ottenuti, senza considerare il limite di 5 secondi imposto dal problema, la soglia di secondi migliore da scegliere è quella di 50 secondi, poiché presenta per frequenza di soluzioni ottime e qualità di soluzioni ottenute il miglioramento più sensibile.\\
Considerando invece il limite di 6 secondi sarebbe possibile risolvere con un buon grado di affidabilità solo istanze con dimensione massima pari a 25.