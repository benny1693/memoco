\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{img/heursolver_diagram.png}
    \caption{Schema delle classi di \texttt{heursolver}}
\end{figure}

Il risolutore euristico è strutturato in 3 parti principali: l'algoritmo di \textit{Local Search} (LS), la funzione di \textit{neighbourhood} e il concetto di successore.\\
Questi tre vengono realizzati dalle classi astratte \texttt{LocalSearch}, \texttt{Neighbourhood} e \texttt{Successor} e vengono poi specializzati in modo da poter fornire un'istanza particolare di un algoritmo LS: nello specifico la versione realizzata sfrutta una funzione di vicinato che ``mescola'' 2-opt e 3-opt.\\
In particolare, la classe \texttt{LSMixed} specializza \texttt{LocalSearch}, le classi \texttt{OPT2Move} e \texttt{OPT3Move} l'interfaccia \texttt{Successor} e le classi \texttt{OPT2}, \texttt{OPT3} e \texttt{OPTMixed} la classe \texttt{Neighbourhood} (Figura 7).\\
Inoltre, il risolutore euristico fa uso di uno script e di \texttt{Parser}, \texttt{TSPProblem} e \texttt{TSPSolution} (già discusse in \S3.3).

\paragraph{L'algoritmo \textit{Local Search}}
L'algoritmo che ho scelto è un \textit{Local Search} con \textit{multistart} casuali.
La ricerca locale non ha nulla di particolare rispetto a quanto visto a lezione: infatti, l'algoritmo cerca di migliorare progressivamente la soluzione trovata a partire da una iniziale (Listing 2).\\
In particolare, la ricerca di un vicino migliorante rispetto alla soluzione attuale viene svolto dalle classi \texttt{OPT2}, \texttt{OPT3} e \texttt{OPTMixed}.

\begin{figure}
    \begin{lstlisting}[escapeinside={^}{^},language={},caption=Pseudocodice di \textit{Local Search}]
LocalSearch(initial_solution)
  ^$x$^ ^$\leftarrow$^ initial_solution
    
  while ^$\exists x' \in N(x) : f(x) < f(x')$^
    ^$x$^ ^$\leftarrow$^ ^$x'$^
    
  return ^$x$^
    \end{lstlisting}
\end{figure}

Dal momento che l'algoritmo fa uso dei \textit{multistart} casuali per diversificare, è sufficiente eseguire un numero stabilito di volte la procedura \texttt{LocalSearch} del Listing 2, partendo ogni volta da soluzioni casuali.\\
Considerando infatti $f(\varnothing) = +\infty$, l'algoritmo (in Listing 3) sceglie la miglior soluzione dopo \texttt{k} iterazioni di \texttt{LocalSearch} a partire da soluzioni iniziali casuali (riga 5), evitando il problema dei minimi locali.\\
In questo caso, l'efficacia dell'algoritmo è fortemente legata alla capacità della funzione di vicinato di reperire delle buone soluzioni.

\begin{figure}
    \begin{lstlisting}[escapeinside={^}{^},language={},caption=Pseudocodice di \textit{Local Search} con \textit{multistart}]
LocalSearchMultistart(k)
  ^\textit{best}^ ^$\leftarrow$^ ^$\varnothing$^

  for i from 1 to k
    ^\textit{actual}^ ^$\leftarrow$^ LocalSearch(random_solution())
    if ^$f(\textit{actual})$^ < ^$f(\textit{best})$^    
      ^\textit{best}^ ^$\leftarrow$^ ^\textit{actual}^

  return ^\textit{best}^
    \end{lstlisting}
\end{figure}

\paragraph{Funzione di vicinato}
La funzione di vicinato utilizza sia 2-opt che 3-opt. Per garantire una maggior rapidità di esecuzione dell'algoritmo, il secondo viene usato solo quando la differenza percentuale di due soluzioni ottenute consecutivamente è inferiore a una certa soglia: siano $\texttt{S}_t$ e $\texttt{var(S}_t\texttt{,S}_{t+1}\texttt{)}$ la variazione da $\texttt{S}_{t}$ a $\texttt{S}_{t+1}$, allora la funzione di vicinato cambia comportamento se
\begin{equation}
    \dfrac{\texttt{S}_{t} + \texttt{var(S}_t\texttt{,S}_{t+1}\texttt{)}}{\texttt{S}_{t}} > R
\end{equation}
dove $R$ è un parametro prestabilito.\\
Se vale la disequazione (1) o se non esiste alcun vicino migliorante usando la politica 2-opt, allora l'algoritmo cerca il miglior successore in
\begin{center}
    $\textit{Neighbours}_{\text{2-opt}}(\texttt{S}_t) \cup \textit{Neighbours}_{\text{3-opt}}(\texttt{S}_t)$.
\end{center}
Altrimenti il miglior successore viene cercato solo in $\textit{Neighbours}_{\text{2-opt}}(\texttt{S}_t)$. Quindi, $R$ influenza la dimensione del vicinato e, conseguentemente, anche il tempo di esecuzione dell'algoritmo.\\
L'idea di base è di ``ampliare" i vicinati ogni qual volta ci si avvicina a un minimo locale per controllare se effettivamente l'esecuzione sta conducendo alla soluzione ottima.

\paragraph{Calibrazione dei parametri}
L'algoritmo che ho proposto ha, quindi, due parametri da calibrare:
\begin{itemize}
    \item la soglia $R$;
    \item il numero di \textit{multistart}.
\end{itemize}
Una volta realizzato l'algoritmo, ho raccolto dati sia in base al parametro $R$, sia in base al numero di \textit{multistart}.\\
Entrambi i parametri influenzano velocità e qualità delle soluzioni dell'algoritmo: perciò è necessario stabilire dei valori che rappresentino un buon compromesso. A tale scopo, ho proceduto nel modo seguente:
\begin{itemize}
    \item soglia $R$:
          \begin{enumerate}
              \item ho fissato il numero di \textit{multistart} (1 solo \textit{start});
              \item ho eseguito il programma con $R \in \{0.0,0.2,0.4,0.6,0.8,1.0\}$;
              \item notato che nell'intervallo $\left[ 0.0,0.8 \right]$ non vi erano radicali miglioramenti del tempo d'esecuzione o peggioramenti della qualità delle soluzioni, ho eseguito l'algoritmo con $R \in \{0.8,0.85,0.9,0.95,1.0.\}$;
              \item dato che i primi cambiamenti sensibili si potevano rilevare a partire da $0.95$, ho effettuato una nuova esecuzione con $R \in \{0.95,0.96,0.97,0.98,0.99,1.0\}$;
              \item tra questi valori, ho scelto quindi $0.97$, che permette di avere un tempo di esecuzione di circa 5 secondi a fronte di una soluzione al massimo peggiore di $0.55$\% rispetto all'ottimo.
          \end{enumerate}
    \item numero di \textit{multistart}:
          \begin{enumerate}
              \item ho fissato la soglia $R$ a 0.97;
              \item ho eseguito il programma con un numero di \textit{multistart} pari a 1, 10, 20, 30, 40, 50;
              \item ho notato che si ha un sensibile miglioramento da 1 a 10 e molto minore da 10 in poi e, quindi, ho scelto di impostare il numero di \textit{multistart} a 10.
          \end{enumerate}
\end{itemize}
Quindi, ho scelto come parametri per l'algoritmo $R = 0.97$ e numero di \textit{multistart} pari a 10.

\paragraph{La classe \texttt{LocalSearch}}
\texttt{LocalSearch} viene costruita a partire da un'istanza di TSP ed è dotata di 2 metodi pubblici:
\begin{itemize}
    \item \texttt{solve(TSPSolution start\_solution)}: restituisce la soluzione al problema fornito durante la costruzione a partire da una soluzione specifica usando l'approccio generale descritto a lezione;
    \item \texttt{random\_multistart\_solve(u\_int n)}: risolve il problema fornito durante la costruzione \texttt{n} volte con \texttt{n} soluzioni iniziali casuali.
\end{itemize}
Tuttavia, il polimorfismo non viene tanto sfruttato dai metodi pubblici, ma più che altro dal metodo protetto \texttt{next\_move(TSPSolution solution)}: infatti, ogni sottoclasse di \texttt{LocalSearch} dovrà fornire la propria definizione di successore.

\paragraph{La classe \texttt{LSMixed}}
\texttt{LSMixed} concretizza \texttt{LocalSearch}, accettando un parametro che stabilisce il valore di $R$ (vedi disequazione (1)).\\
Il successore è, quindi, definito come stabilito dalla classe \texttt{OPTMixed}.

\paragraph{La classe \texttt{Neighbourhood}}
\texttt{Neighbourhood} fornisce una rappresentazione del vicinato di una data soluzione di un determinato problema TSP (forniti durante la costruzione).\\
La classe è fornita solo del metodo pubblico (e astratto) \texttt{findMove()}, che restituisce il successore della soluzione attuale in base al tipo di vicinato scelto.

\paragraph{La classe \texttt{OPT2}}
\texttt{OPT2} concretizza \texttt{Neighbourhood} e ridefinisce la funzione \texttt{findMove} in modo da restituire la miglior mossa 2-opt a partire dalla soluzione fornita durante la costruzione dell'oggetto.\\
Nel caso in cui non esista alcun vicino che migliori la soluzione attuale, allora restituisce un puntatore nullo.

\paragraph{La classe \texttt{OPT3}}
\texttt{OPT3} concretizza \texttt{Neighbourhood} e ridefinisce la funzione \texttt{findMove} in modo da restituire la miglior mossa 3-opt a partire dalla soluzione fornita durante la costruzione dell'oggetto.\\
Nel caso in cui non esista alcun vicino che migliori la soluzione attuale, allora restituisce un puntatore nullo.

\paragraph{La classe \texttt{OPTMixed}}
\texttt{OPTMixed} concretizza \texttt{Neighbourhood} e ridefinisce la funzione \texttt{findMove} in modo da restituire il miglior vicino in $\textit{Neighbours}_{\text{2-opt}}(\texttt{S}_t) \cup \textit{Neighbours}_{\text{3-opt}}(\texttt{S}_t)$ se vale la disequazione (1) o se non esistono vicini miglioranti in $\textit{Neighbours}_{\text{2-opt}}(\texttt{S}_t)$. Altrimenti, restituisce il miglior vicino in $\textit{Neighbours}_{\text{2-opt}}(\texttt{S}_t)$.\\
Nel caso in cui non esista alcun vicino che migliori la soluzione attuale, allora restituisce un puntatore nullo.\\
La disequazione (1) vale se e solo se
\begin{equation}
    \dfrac{\texttt{var(S}_t\texttt{,S}_{t+1}\texttt{)}}{\texttt{S}_{t}} > R - 1
\end{equation}
che è più semplice e meno prona a errori di precisione da parte del calcolatore.

\paragraph{L'interfaccia \texttt{Successor}}
\texttt{Successor} è un'interfaccia che mette a disposizione due metodi:
\begin{itemize}
    \item \texttt{apply(TSPProblem problem, TSPSolution solution)}: metodo che sostituisce la soluzione attuale con quella del successore;
    \item \texttt{variation(TSPProblem problem, TSPSolution solution)}: metodo che restituisce la variazione del costo della soluzione.
\end{itemize}

\paragraph{La classe \texttt{OPT2Move}}
\texttt{OPT2Move} modella una mossa di un vicinato 2-opt, a partire da una soluzione di partenza, da un'istanza TSP e da due indici \texttt{i} e \texttt{j}.\\
La funzione \texttt{variation} restituisce in tempo costante la variazione tra le due soluzioni e la funzione \texttt{apply} sfrutta la rappresentazione della soluzione come sequenza di nodi e inverte i nodi tra le posizioni \texttt{i} e \texttt{j} in tempo lineare come visto in aula.

\paragraph{La classe \texttt{OPT3Move}}
\texttt{OPT3Move} modella una mossa per 3-opt, a partire da una soluzione di partenza, da un'istanza TSP e da tre indici \texttt{i}, \texttt{j} e \texttt{k}. Tuttavia, questo non è sufficiente a identificare univocamente una mossa: infatti, dati tre indici è possibile notare che vi sono generalmente 4 mosse possibili (Figura 8). Esiste un caso particolare in cui vi è una sola mossa disponibile: quando due degli archi da eliminare sono contigui l'unica possibilità è quella mostrata in Figura 9.\\
In generale, \texttt{OPT3Move} sceglie la migliore delle 4 mosse possibili.\\
Quindi, la classe \texttt{OPT3Move} ridefinisce i metodi pubblici di \texttt{Successor} nel modo seguente:
\begin{itemize}
    \item
          \texttt{apply(TSPProblem problem, TSPSolution solution)}: date le 4 mosse possibili, sceglie quella con la miglior variazione e la applica.\\
          Dal punto di vista implementativo, è possibile procedere in modo simile a come si è già visto per 2-opt. Infatti, considerando la soluzione come sequenza di nodi formata come segue
          \begin{center}
              $... h i ... j m... k l ...$,
          \end{center}
          allora le quattro possibilità si traducono nelle seguenti trasformazioni:
          \begin{enumerate}
              \item $...hj...ik...ml...$, cioè inversione di $i...j$ e inversione di $m...k$;
              \item $...hm...ki...jl...$, cioè scambio tra $i...j$ e $m...k$;
              \item $...hk...mi...jl...$, cioè inversione di $i...k$ e successiva inversione di $j...i$;
              \item $...hm...kj...il...$, cioè inversione di $i...k$ e successiva inversione di $k...m$.
          \end{enumerate}
          \`E possibile realizzare le mosse in modo molto semplice usando 2-opt. Usare questo approccio per l'opzione 2 sarebbe meno efficiente rispetto a una semplice traslazione di $i...j$ al posto di $m...k$ e viceversa.\\
          In ogni caso, l'applicazione della mossa avviene sempre in tempo lineare.
    \item
          \texttt{variation(TSPProblem problem, TSPSolution solution)}: il calcolo della variazione avviene in modo simile rispetto a \texttt{OPT2Move}. L'unica differenza è che bisogna calcolare quattro variazioni e restituire la migliore.
\end{itemize}


\begin{figure}
    \centering
    \includegraphics[width=0.5\textwidth]{img/3opt.png}
    \caption{Possibili mosse con 3-opt}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[scale=0.5]{img/3opt_contiguous.png}
    \caption{Unica mossa possibile con 3-opt con archi contigui}
\end{figure}