CC = g++
CPPFLAGS = -g -Wall -O2
LDFLAGS =

CPX_BASE    = /opt/ibm/ILOG/CPLEX_Studio128
CPX_INCDIR  = $(CPX_BASE)/cplex/include
CPX_LIBDIR  = $(CPX_BASE)/cplex/lib/x86-64_linux/static_pic
CPX_LDFLAGS = -lcplex -lm -pthread -ldl

BUILD_DIR = build

OBJ_SOLVER = cplexsolver.o Parser.o TSPProblem.o CPLEXSolver.o TSPSolution.o utilities.o
 
OBJ_GEN = inst_gen.o instances_generator.o utilities.o

OBJ_HEUR = heursolver.o OPT2Move.o OPT3Move.o Parser.o TSPProblem.o TSPSolution.o Neighbourhood.o OPT2.o OPT3.o OPTMixed.o LocalSearch.o LSMixed.o utilities.o

all: build
	rm -rf $(BUILD_DIR)

build: dir inst_gen cplexsolver heursolver

dir:
		mkdir -p $(BUILD_DIR)

%.o: src/%.cpp
		$(CC) $(CPPFLAGS) -I$(CPX_INCDIR) -c $^ -o $(BUILD_DIR)/$@

cplexsolver: $(OBJ_SOLVER) dir
		$(CC) $(CPPFLAGS) $(addprefix $(BUILD_DIR)/,$(OBJ_SOLVER)) -o cplexsolver.out -L$(CPX_LIBDIR) $(CPX_LDFLAGS)

inst_gen: $(OBJ_GEN) dir
		$(CC) $(CPPFLAGS) $(addprefix $(BUILD_DIR)/,$(OBJ_GEN)) -o inst_gen.out

heursolver: $(OBJ_HEUR) dir
		$(CC) $(CPPFLAGS) $(addprefix $(BUILD_DIR)/,$(OBJ_HEUR)) -o heursolver.out

clean:
		rm -rf $(OBJ_HEUR) heursolver.out
		rm -rf $(OBJ_SOLVER) cplexsolver.out
		rm -rf $(OBJ_GEN) inst_gen.out
		rm -rf $(BUILD_DIR)	

.PHONY: clean
