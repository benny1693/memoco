# Methods and Models for Combinatorial Optimization Project
This repo contains the two ways to solve the TSP problem: the first one use the CPLEX Callable library and returns the exact solutions for the problem instances, the second one use a Local Search algorithm with variable neighbourhood.

This project has been developed for the Methods and Models for Combinatorial Optimization course attended during the MSc class in Computer Science of the University of Padua.
