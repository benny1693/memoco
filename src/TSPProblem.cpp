/**
 * @file TSPProblem.cpp
 * @author Benedetto Cosentino (benedetto.cosentino@studenti.unipd.it)
 * @brief Implementation file for the TSPProblem
 * @version 1
 * @date 2020-12-31
 *
 * @copyright Copyright (c) 2020
 *
 */
#include "../include/TSPProblem.hpp"

TSPProblem::TSPProblem(const vector<Position> &holePositions)
    : costs(vector<vector<double>>(holePositions.size(), vector<double>())) {
  for (u_int i = 0; i < holePositions.size(); ++i) {
    costs[i] = vector<double>(holePositions.size() - i - 1, 0);
    for (u_int j = i + 1; j < holePositions.size(); j++) {
      costs[i][j - i - 1] =
          abs(holePositions[i].first - holePositions[j].first) +
          abs(holePositions[i].second - holePositions[j].second);
    }
  }
}

double TSPProblem::c(u_int i, u_int j) const {
  if (i == j)
    return 0;
  if (i < j)
    return costs[i][j - i - 1];
  return costs[j][i - j - 1];
}

u_int TSPProblem::n() const { return costs.size(); }