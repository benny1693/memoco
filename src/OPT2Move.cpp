/**
 * @file OPT2Move.cpp
 * @author Benedetto Cosentino (benedetto.cosentino@studenti.unipd.it)
 * @brief Implementation file for class OPT2Move
 * @version 1
 * @date 2021-01-02
 *
 * @copyright Copyright (c) 2021
 *
 */

#include "../include/OPT2Move.hpp"

OPT2Move::OPT2Move(u_int i, u_int j) : from(i), to(j) {
  if (i < 1 || i >= j)
    throw std::runtime_error("Invalid 2-OPT move: wrong indexes (" +
                             std::to_string(i) + "," + std::to_string(j) + ")");
}

void OPT2Move::apply(const TSPProblem &problem, TSPSolution &solution) const {
  if (to >= problem.n())
    throw std::runtime_error(
        "Index j out of bounds for this instance: j = " + std::to_string(to) +
        ", " + "N = " + std::to_string(problem.n()));

  solution.cost += variation(problem, solution);
  for (u_int i = from; i <= (to + from) / 2; ++i)
    std::swap(solution.nodes[i], solution.nodes[to - i + from]);
}

double OPT2Move::variation(const TSPProblem &problem,
                           const TSPSolution &solution) const {

  u_int h = solution.nodes[from - 1], i = solution.nodes[from],
        j = solution.nodes[to], l = solution.nodes[(to + 1) % problem.n()];

  return -problem.c(h, i) - problem.c(j, l) + problem.c(h, j) + problem.c(i, l);
}