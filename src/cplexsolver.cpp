/**
 * @file cplexsolver.cpp
 * @author Benedetto Cosentino (benedetto.cosentino@studenti.unipd.it)
 * @brief Script for the computation of the solutions of the generated
 *        instances with CPLEX library
 * @version 1
 * @date 2020-12-31
 *
 * @copyright Copyright (c) 2020
 *
 */

#include "../include/CPLEXSolver.hpp"

#include <sys/stat.h>

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "../include/Parser.hpp"
#include "../include/cpxmacro.h"
#include "../include/utilities.hpp"

using std::vector;
using namespace utils;

int main(int argc, char const *argv[]) {
  // argv =   exec_name, [ time_limit ]

  // time limit in seconds
  double time_limit = 0.0;

  if (argc >= 2)
    time_limit = to_<double>(argv[1]);

  std::cout << "Start solving the problems with the following parameters:\n";
  if (time_limit)
    std::cout << "Time limit (s):\t\t" << time_limit << std::endl;
  else
    std::cout << "Time limit (s):\t\tNone" << std::endl;

  std::string results_name =
      "results" + (time_limit ? "_" + std::to_string(time_limit) : "");
  mkdir("results", 0777);

  std::ofstream res_file("results/" + results_name + ".txt");

  for (std::string instance_name : listFiles("instances")) {
    Parser p;

    try {
      TSPProblem prob = p.readFile("instances/" + instance_name);

      CPLEXSolver solver(prob);

      if (time_limit)
        solver.setTimeLimit(time_limit);

      solver.solve();

      TSPSolution solution;

      Time begin = std::chrono::steady_clock::now();
      try {
        solution = solver.generate_solution();
        std::cout << solution;
      } catch (std::runtime_error e) {
        std::cout << "No solution exists" << std::endl;
        solution.cost = -1;
      }
      Time end = std::chrono::steady_clock::now();

      double time_elapsed_solution =
          std::chrono::duration_cast<std::chrono::microseconds>(end - begin)
              .count();

      std::cout << "Time elapsed (ms): "
                << (solver.time_elapsed() + time_elapsed_solution) / 1e3
                << std::endl;

      // print
      res_file << instance_name << "\t" << prob.n() << "\t" << time_limit
               << "\t" << solver.time_elapsed() << "\t" << solution.cost
               << std::endl;
    } catch (std::runtime_error e) {
      std::cout << e.what() << std::endl;
    }
    std::cout << std::endl;
  }
  res_file.close();
  std::cout << "end\n";
}
