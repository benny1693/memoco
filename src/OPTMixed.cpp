/**
 * @file OPTMixed.cpp
 * @author Benedetto Cosentino (benedetto.cosentino@studenti.unipd.it)
 * @brief Implementation file for OPTMixed class
 * @version 1
 * @date 2021-01-04
 *
 * @copyright Copyright (c) 2021
 *
 */

#include "../include/OPTMixed.hpp"

OPTMixed::OPTMixed(const TSPProblem &p, const TSPSolution &s, double r)
    : Neighbourhood(p, s), rate(r) {}

std::unique_ptr<Successor> OPTMixed::findMove() const {

  auto opt2move = OPT2(problem, solution).findMove();

  // (cost + variation)/cost = 1 + variation/cost
  // Since variation < 0, 1 + variation/cost < 1
  // Now, (cost + variation)/cost <= rate.
  // Hence, (cost + variation)/cost - 1 = variation/cost <= rate - 1
  // Then, if a neighbour is found in 2-OPT, just checks if
  //          variation/cost <= rate - 1
  if (opt2move &&
      opt2move->variation(problem, solution) / solution.cost <= rate - 1) {
    return opt2move;
  }

  // there's no improving neighbour in 2-OPT neighbour or
  // (variation/cost) > rate - 1
  // Choose the best move from 2-OPT and 3-OPT
  auto opt3move = OPT3(problem, solution).findMove();
  if (opt2move && opt3move &&
      opt2move->variation(problem, solution) <=
          opt3move->variation(problem, solution))
    return opt2move;

  return opt3move;
}