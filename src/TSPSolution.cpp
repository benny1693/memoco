/**
 * @file TSPSolution.cpp
 * @author Benedetto Cosentino (benedetto.cosentino@studenti.unipd.it)
 * @brief Implementation of TSPSolution
 * @version 1
 * @date 2020-12-31
 *
 * @copyright Copyright (c) 2020
 *
 */
#include "../include/TSPSolution.hpp"

std::ostream &operator<<(std::ostream &out, const TSPSolution &sol) {
  out << "Solution path: (";
  for (auto node : sol.nodes)
    out << node << ", ";
  out << "\b\b)" << std::endl;

  out << "Objective function optimum value (min): " << sol.cost << std::endl;

  return out;
}