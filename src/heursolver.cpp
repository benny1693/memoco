/**
 * @file heursolver.cpp
 * @author Benedetto Cosentino (benedetto.cosentino@studenti.unipd.it)
 * Script for the computation of the solutions of the generated
 *        instances with a Local Search approach
 * @version 1
 * @date 2021-01-03
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <sys/stat.h>

#include <chrono>
#include <iostream>
#include <string>
#include <vector>

#include "../include/LSMixed.hpp"
#include "../include/Parser.hpp"
#include "../include/TSPProblem.hpp"
#include "../include/TSPSolution.hpp"
#include "../include/utilities.hpp"

using std::string;
using std::to_string;
using std::vector;
using namespace utils;

int main(int argc, char *argv[]) {
  // argv =   exec_name, [ rate, n_multistart ]

  double rate = 0.97;
  int n_multistart = 10;

  if (argc >= 2) {
    rate = to_<double>(argv[1]);
    if (argc >= 3)
      n_multistart = to_<int>(argv[2]);
  }

  std::cout << "Start solving the problems with the following parameters:\n";
  std::cout << "Rate (cost + variation)/cost:\t\t" << rate << std::endl;
  std::cout << "Number of restarts:\t\t\t" << n_multistart << std::endl;

  string results_name =
      "results_" + to_string(rate) + "_" + to_string(n_multistart);

  mkdir("results_heur", 0777);
  std::ofstream res_file("results_heur/" + results_name + ".txt");

  int i = 0;
  for (string instance_name : listFiles("instances")) {
    std::cout << ((++i) / 5.7) << std::endl;
    Parser p;

    try {
      TSPProblem prob = p.readFile("instances/" + instance_name);

      LocalSearch *solver = new LSMixed(prob, rate);

      Time begin = std::chrono::steady_clock::now();
      TSPSolution sol = solver->random_multistart_solve(n_multistart);
      Time end = std::chrono::steady_clock::now();

      double time_elapsed =
          std::chrono::duration_cast<std::chrono::microseconds>(end - begin)
              .count();

      std::cout << sol << "Time elapsed (ms): " << time_elapsed / 1e3
                << std::endl;

      // print
      res_file << instance_name << "\t" << prob.n() << "\t" << rate << "\t"
               << n_multistart << "\t" << time_elapsed << "\t" << sol.cost
               << std::endl;
    } catch (std::runtime_error e) {
      std::cout << e.what() << std::endl;
    }
    std::cout << std::endl;
  }
  res_file.close();
  std::cout << "end\n";
}