/**
 * @file OPT3.cpp
 * @author Benedetto Cosentino (benedetto.cosentino@studenti.unipd.it)
 * @brief Implementation file for OPT3 class
 * @version 1
 * @date 2021-01-04
 *
 * @copyright Copyright (c) 2021
 *
 */

#include "../include/OPT3.hpp"

OPT3::OPT3(const TSPProblem &p, const TSPSolution &s) : Neighbourhood(p, s) {}

std::unique_ptr<Successor> OPT3::findMove() const {

  u_int best_i, best_j, best_k;
  best_i = best_j = best_k = 0;

  double best_var = 0;

  for (u_int i = 1; i < problem.n() - 2; ++i) {
    for (u_int j = i; j < problem.n() - 1; ++j) {
      for (u_int k = j + 1; k < problem.n(); ++k) {
        if (i < j || k > j + 1) {
          double actual_var = OPT3Move(i, j, k).variation(problem, solution);
          if (actual_var < best_var) {
            best_i = i;
            best_j = j;
            best_k = k;
            best_var = actual_var;
          }
        }
      }
    }
  }

  if (best_var == 0)
    return 0; // if no improving move is found

  return std::unique_ptr<Successor>(new OPT3Move(best_i, best_j, best_k));
}