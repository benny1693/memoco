/**
 * @file inst_gen.cpp
 * @author Benedetto Cosentino (benedetto.cosentino@studenti.unipd.it)
 * @brief Script for the generation of the TSP instances
 * @version 1
 * @date 2020-12-31
 *
 * @copyright Copyright (c) 2020
 *
 */

#include <sys/stat.h>

#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

#include "../include/instances_generator.hpp"
#include "../include/utilities.hpp"

using namespace utils;

int main(int argc, char const *argv[]) {
  // argv =   exec_name,  instance_name,
  //        [ first_N,    last_N, step_N, n_instances_for_N,
  //          Xlimit, Ylimit, max_element_per_X ]

  // optional arguments
  std::vector<u_int> args = {25, 25, 1, 1, 100, 100, 1};

  if (argc < 2) {
    std::cerr << "Usage: ./main.out instance_name [optionals]" << std::endl;
    return 0;
  } else {
    for (u_int i = 0; (i < args.size()) && (argc > (int)(i + 2)); ++i) {
      args[i] = to_<u_int>(argv[i + 2]);
    }

    if (args[0] > args[1])
      args[1] = args[0];
    if (args[4] > args[5])
      args[5] = args[4];
  }

  std::cout
      << "Start generating the instances with the following parameters:\n";
  std::cout << "Smaller instance:\t\t" << args[0] << std::endl
            << "Bigger instance:\t\t" << args[1] << std::endl
            << "Step of |N|:\t\t\t" << args[2] << std::endl
            << "Instances for each |N|:\t\t" << args[3] << std::endl;

  std::cout << "Limit for X coordinate:\t\t" << args[4] << std::endl
            << "Limit for Y coordinate:\t\t" << args[5] << std::endl
            << "Maximum number of Y for each X:\t" << args[6] << std::endl
            << std::endl;

  std::string instances_dirname = ("instances");
  mkdir(instances_dirname.c_str(), 0777);

  for (; args[0] <= args[1]; args[0] += args[2]) {
    std::string instance_name = argv[1] + ("_" + std::to_string(args[0]));
    InstancesGenerator gen(instance_name, instances_dirname, args[0], args[4],
                           args[5], args[6]);

    gen.generate_multiple_instances(args[3]);

    std::cout << std::endl;
  }
  std::cout << "end\n";
}
