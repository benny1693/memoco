/**
 * @file utilities.cpp
 * @author Benedetto Cosentino (benedetto.cosentino@studenti.unipd.it)
 * @brief Implementation file for the utilities
 * @version 1
 * @date 2021-01-06
 *
 * @copyright Copyright (c) 2021
 *
 */

#include "../include/utilities.hpp"

vector<string> utils::listFiles(string dir_name) {
  DIR *dir;
  struct dirent *ent;
  vector<string> files;

  if ((dir = opendir(dir_name.c_str())) != NULL) {
    /* print all the files and directories within directory */
    while ((ent = readdir(dir)) != NULL)
      if (ent->d_name[0] != '.')
        files.push_back(ent->d_name);
    closedir(dir);
  } else {
    /* could not open directory */
    throw std::runtime_error("Directory \"instances\" doesn't exists");
  }

  return files;
}