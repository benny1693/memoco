/**
 * @file instances_generator.cpp
 * @author Benedetto Cosentino (benedetto.cosentino@studenti.unipd.it)
 * @brief Implementation file for InstancesGenerator
 * @version 1
 * @date 2020-12-31
 *
 * @copyright Copyright (c) 2020
 *
 */
#include "../include/instances_generator.hpp"

InstancesGenerator::InstancesGenerator(std::string name, u_int N, u_int max_x,
                                       u_int max_y, u_int max_holes_per_row)
    : instances_name(name), n_nodes(N), limit_x(max_x), limit_y(max_y),
      maximum_holes(max_holes_per_row + 1) {

  n_nodes = n_nodes < limit_x * limit_y ? n_nodes : limit_x * limit_y;
  if (maximum_holes < 2)
    throw std::runtime_error(
        "Number of nodes for each coordinate x not sufficient");
  if (limit_x < n_nodes || limit_y < n_nodes)
    throw std::runtime_error(
        "Limit for x and y lower then the number of nodes");
}

InstancesGenerator::InstancesGenerator(std::string name, std::string dname,
                                       u_int N, u_int max_x, u_int max_y,
                                       u_int max_holes_per_row)
    : InstancesGenerator(name, N, max_x, max_y, max_holes_per_row) {
  dir_name = dname;
}

void InstancesGenerator::generate_single_instance(u_int seed) {
  if (dir_name == "")
    std::cout << "Generating instance \"" << instances_name << "\"... "
              << std::flush;
  else
    std::cout << "Generating instance \"" << instances_name
              << "\" in directory \"" << dir_name << "\"... " << std::flush;

  srand(time(NULL) + seed);

  u_int remaining_nodes = 1;

  remaining_nodes = n_nodes;

  std::ofstream output(dir_name + "/" + instances_name + ".txt");

  output << "BEGIN\n";

  std::vector<u_int> xs(n_nodes);

  // Choose randomly the xs positions
  for (u_int i = 0; i < n_nodes; ++i)
    xs[i] = rand() % limit_x;

  while (xs.size() > 0 && remaining_nodes > 0) {
    // Choose randomly from  xs
    u_int i = rand() % xs.size();
    u_int x = xs[i];

    // Choose randomly the number of positions (x,y) for y ∈ ys
    // y_n should be at least 1
    // Then decrement y_n if exceeds the number of the nodes left
    u_int y_n = ((rand() % n_nodes) % limit_y) % maximum_holes;
    if (!y_n)
      y_n++;
    y_n = y_n < remaining_nodes ? y_n : remaining_nodes;

    // Extract x from xs
    xs.erase(xs.begin() + i);

    remaining_nodes -= y_n;

    std::unordered_set<u_int> ys;

    while (ys.size() < y_n) {
      // Choose randomly the y for (x,y)
      u_int y = rand() % limit_y;
      if (ys.insert(y).second)
        output << x << " " << y << std::endl;
    }
  }
  output << "END\n";
  output.close();

  std::cout << "completed!" << std::endl;
}

void InstancesGenerator::generate_multiple_instances(u_int n_instances) {

  std::string old_name = instances_name;

  for (u_int i = 1; i <= n_instances; i++) {
    instances_name = old_name + "_" + std::to_string(i);
    generate_single_instance(i);
  }

  instances_name = old_name;
}