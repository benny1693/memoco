/**
 * @file OPT2.cpp
 * @author Benedetto Cosentino (benedetto.cosentino@studenti.unipd.it)
 * @brief Implementation file for OPT2 class
 * @version 1
 * @date 2021-01-04
 *
 * @copyright Copyright (c) 2021
 *
 */

#include "../include/OPT2.hpp"

OPT2::OPT2(const TSPProblem &p, const TSPSolution &s) : Neighbourhood(p, s) {}

std::unique_ptr<Successor> OPT2::findMove() const {

  u_int best_i, best_j;
  best_i = best_j = 0;

  double best_var = 0;

  for (u_int i = 1; i < problem.n() - 1; ++i) {
    for (u_int j = i + 1; j < problem.n() - 1; ++j) {
      double actual_var = OPT2Move(i, j).variation(problem, solution);
      if (actual_var < best_var) {
        best_i = i;
        best_j = j;
        best_var = actual_var;
      }
    }
  }

  if (best_var == 0)
    return 0; // if no improving move is found

  return std::unique_ptr<Successor>(new OPT2Move(best_i, best_j));
}