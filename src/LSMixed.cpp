/**
 * @file LSMixed.cpp
 * @author Benedetto Cosentino (benedetto.cosentino@studenti.unipd.it)
 * @brief  Implementation file for LSMixed class
 * @version 1
 * @date 2021-01-05
 *
 * @copyright Copyright (c) 2021
 *
 */

#include "../include/LSMixed.hpp"

std::unique_ptr<Successor>
LSMixed::next_move(const TSPSolution &solution) const {
  return OPTMixed(problem, solution, rate).findMove();
}

LSMixed::LSMixed(const TSPProblem &p, double r) : LocalSearch(p), rate(r) {
  if (rate < 0.0 || rate > 1.0)
    throw std::runtime_error("The threshold rate is not between 0 and 1");
}
