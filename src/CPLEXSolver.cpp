/**
 * @file CPLEXSolver.cpp
 * @author Benedetto Cosentino (benedetto.cosentino@studenti.unipd.it)
 * @brief Implementation file for CPLEXSolver
 * @version 1
 * @date 2020-12-31
 *
 * @copyright Copyright (c) 2020
 *
 */

#include "../include/CPLEXSolver.hpp"

CPLEXSolver::CPLEXSolver(const TSPProblem &problem) {

  std::cout << "Setting up enviroment and problem in CPLEX... " << std::flush;
  ASSIGN_ENV(env);
  ASSIGN_PROB(env, lp);

  N = problem.n();

  // Variables setup

  // |X| = tuples(i,j) - tuples(i,0) - tuples(i,i) =
  //     = tuples(i,j) - tuples(i,0) - (tuples(i,i) s.t. i!=0 - tuple(0,0))
  // |X| = N^2 - N - (N - 1)
  X.resize(N * N - N - N + 1);

  // |Y| = tuples(i,j) - tuples(i,i)
  // |Y| = N^2 - N
  Y.resize(N * N - N);

  u_int vars = 0;

  // x variables
  for (u_int k = 0; k < N * N; ++k) {
    int i = k / N, j = k % N;
    if (i != j && j > 0) {
      char xtype = 'I';
      double obj = 0.0;
      double lb = 0.0;
      double ub = CPX_INFBOUND;
      char *xname = const_cast<char *>(
          ("x_" + std::to_string(i) + "," + std::to_string(j)).c_str());
      CHECKED_CPX_CALL(CPXnewcols, env, lp, 1, &obj, &lb, &ub, &xtype, &xname);

      x(i, j) = vars++;
    }
  }

  // y variables
  for (u_int k = 0; k < N * N; ++k) {
    int i = k / N, j = k % N;
    if (i != j) {
      char xtype = 'B';
      double obj = problem.c(i, j);
      double lb = 0.0;
      double ub = 1.0;
      char *yname = const_cast<char *>(
          ("y_" + std::to_string(i) + "," + std::to_string(j)).c_str());
      CHECKED_CPX_CALL(CPXnewcols, env, lp, 1, &obj, &lb, &ub, &xtype, &yname);

      y(i, j) = vars++;
    }
  }

  // Constraints setup

  // Demand constraint
  // (10) sum_i(x_i,k) - sum_j(x_k,j) = 1 for all 0 < k <= N
  for (u_int k = 1; k < N; ++k) {
    // exclude (i,i) and (k,0) for the second sum
    // (N - 1) elements in the first sum
    // (N - 2) elements in the second one
    // (2N - 3) total elements
    vector<int> indexes(2 * N - 3);
    vector<double> coeff(2 * N - 3, 1);

    for (u_int i = 0; i < N; ++i) {
      if (i != k) {
        indexes[i - (i > k)] = x(i, k);
      }
    }

    for (u_int j = 1; j < N; ++j) {
      if (j != k) {
        indexes[N + j - 2 - (j > k)] = x(k, j);
        coeff[N + j - 2 - (j > k)] *= -1;
      }
    }

    char sense = 'E';
    double rhs = 1;
    char *name = const_cast<char *>(("d_" + std::to_string(k)).c_str());
    int matbeg = 0;
    CHECKED_CPX_CALL(CPXaddrows, env, lp, 0, 1, 2 * N - 3, &rhs, &sense,
                     &matbeg, &indexes[0], &coeff[0], NULL, &name);
  }

  // Outflow and inflow constraint
  // sum_j(y_i,j) = 1 for all 0 <= i < N -> outflow
  // sum_i(y_i,j) = 1 for all 0 <= j < N -> inflow
  // Just invert (i, j) to obtain the inflow constraint
  for (u_int i = 0; i < N; ++i) {
    // exclude (i,i)
    // (N - 1) elements needed for both the sums
    vector<int> indexes_out(N - 1);
    vector<int> indexes_in(N - 1);
    vector<double> coeff(N - 1, 1); // we can reuse the same coefficients

    for (u_int j = 0; j < N; ++j) {
      if (i != j) {
        indexes_out[j - (j > i)] = y(i, j);
        indexes_in[j - (j > i)] = y(j, i);
      }
    }

    char sense = 'E';
    double rhs = 1;
    int matbeg = 0;

    char *name = const_cast<char *>(("out_" + std::to_string(i)).c_str());
    CHECKED_CPX_CALL(CPXaddrows, env, lp, 0, 1, N - 1, &rhs, &sense, &matbeg,
                     &indexes_out[0], &coeff[0], NULL, &name);

    name = const_cast<char *>(("in_" + std::to_string(i)).c_str());
    CHECKED_CPX_CALL(CPXaddrows, env, lp, 0, 1, N - 1, &rhs, &sense, &matbeg,
                     &indexes_in[0], &coeff[0], NULL, &name);
  }

  // x - y relations
  // x_i,j <= (N-1)y_i,j for all (i,j) and j!=0
  for (u_int i = 0; i < N; ++i) {
    for (u_int j = 1; j < N; ++j) {
      if (i != j) {
        int indexes[2] = {(int)x(i, j), (int)y(i, j)};
        double coeff[2] = {1.0, ((-(double)N) + 1.0)};

        char sense = 'L';
        double rhs = 0;
        char *name = const_cast<char *>(
            ("rel_" + std::to_string(i) + "," + std::to_string(j)).c_str());
        int matbeg = 0;
        CHECKED_CPX_CALL(CPXaddrows, env, lp, 0, 1, 2, &rhs, &sense, &matbeg,
                         indexes, coeff, NULL, &name);
      }
    }
  }
  std::cout << "completed!" << std::endl;
}

CPLEXSolver::~CPLEXSolver() {
  CPXfreeprob(env, &lp);
  CPXcloseCPLEX(&env);
}

void CPLEXSolver::setTimeLimit(double time_limit) {
  CPXsetdblparam(env, CPX_PARAM_TILIM, time_limit);
}

u_int &CPLEXSolver::x(u_int i, u_int j) {
  if (i == j) {
    throw std::runtime_error("Tried to access variable x_" + std::to_string(i) +
                             "," + std::to_string(j) + ": inexistent");
  }
  return X[i * N + j - i - i - (j > i)];
}

u_int &CPLEXSolver::y(u_int i, u_int j) {
  if (i == j) {
    throw std::runtime_error("Tried to access variable y_" + std::to_string(i) +
                             "," + std::to_string(j) + ": inexistent");
  }
  return Y[i * N + j - i - (j > i)];
}

void CPLEXSolver::printModel(string namefile) {
  CHECKED_CPX_CALL(CPXwriteprob, env, lp, namefile.c_str(), 0);
}

void CPLEXSolver::printSolution(string namefile) {
  CHECKED_CPX_CALL(CPXsolwrite, env, lp, namefile.c_str());
}

void CPLEXSolver::solve() {
  std::cout << "Solving the problem... " << std::flush;
  // Solve the problem with CPLEX
  Time begin = std::chrono::steady_clock::now();
  CHECKED_CPX_CALL(CPXmipopt, env, lp);
  Time end = std::chrono::steady_clock::now();
  time_el = end - begin;
  std::cout << "completed!" << std::endl;
}

TSPSolution CPLEXSolver::generate_solution() {
  std::cout << "Computing the solution... " << std::flush;

  TSPSolution solution;

  // Find the optimum value of the objective function
  CHECKED_CPX_CALL(CPXgetobjval, env, lp, &(solution.cost));

  solution.nodes = generate_path();

  std::cout << "completed!" << std::endl;

  return solution;
}

double CPLEXSolver::time_elapsed() const {
  return std::chrono::duration_cast<std::chrono::microseconds>(time_el).count();
}

vector<u_int> CPLEXSolver::generate_path() {

  int num_vars = CPXgetnumcols(env, lp);
  std::vector<double> ys;

  ys.resize(num_vars - X.size());
  int starting_idx = X.size();
  int ending_idx = num_vars - 1;
  CHECKED_CPX_CALL(CPXgetx, env, lp, &ys[0], starting_idx, ending_idx);

  // ys
  // each row has N-1 elements
  // (0,1)    (0,2)    (0,3)   ... (0,N-1)
  // (1,0)    (1,2)    (1,3)   ... (1,N-1)
  // (2,0)    (2,1)    (2,3)   ... (2,N-1)
  // ...
  // (N-1,0)  (N-1,1)  (N-1,2) ... (N-1,N-2)

  vector<u_int> res(N);
  res[0] = 0;

  u_int i = 0, j = 0;
  for (u_int k = 0; k < N - 1; ++k) {
    // search the tuple such that y_i,j = 1
    j = 0;
    for (; j < N - 1 && (ys[i * (N - 1) + j] < 0.0001); ++j)
      ;
    j += (j >= i); // conversion to the original index of j

    i = j; // the next first endpoint of the edge is j
    res[k + 1] = j;
  }

  return res;
}