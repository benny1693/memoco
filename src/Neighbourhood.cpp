/**
 * @file Neighbourhood.cpp
 * @author Benedetto Cosentino (benedetto.cosentino@studenti.unipd.it)
 * @brief Implementation file for the class Neighbourhood
 * @version 1
 * @date 2021-01-04
 *
 * @copyright Copyright (c) 2021
 *
 */

#include "../include/Neighbourhood.hpp"

Neighbourhood::Neighbourhood(const TSPProblem &p, const TSPSolution &s)
    : problem(p), solution(s) {}
