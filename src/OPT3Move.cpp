/**
 * @file OPT3Move.cpp
 * @author Benedetto Cosentino (benedetto.cosentino@studenti.unipd.it)
 * @brief Implementation file for class OPT3Move
 * @version 1
 * @date 2021-01-02
 *
 * @copyright Copyright (c) 2021
 *
 */

#include "../include/OPT3Move.hpp"

OPT3Move::OPT3Move(u_int i, u_int j, u_int k) : from(i), through(j), to(k) {
  // if there are three arcs contiguous arcs then the class cannot find
  // a neighbour
  // Contiguity condition: i == j and k == j + 1
  if (i < 1 || i > j || j >= k || (i == j && k == j + 1))
    throw std::runtime_error("Invalid 3-OPT move: wrong indexes (" +
                             std::to_string(i) + "," + std::to_string(j) + "," +
                             std::to_string(k) + ")");
}

vector<double> OPT3Move::vars(const TSPProblem &problem,
                              const TSPSolution &solution) const {
  u_int h = solution.nodes[from - 1],             // i - 1
      i = solution.nodes[from],                   // i
      j = solution.nodes[through],                // j
      m = solution.nodes[through + 1],            // j + 1
      k = solution.nodes[to],                     // k
      l = solution.nodes[(to + 1) % problem.n()]; // k + 1

  double old_edges = problem.c(h, i) + problem.c(j, m) + problem.c(k, l);

  vector<double> v(4, -old_edges);

  v[0] += problem.c(h, j) + problem.c(i, k) + problem.c(m, l);
  v[1] += problem.c(h, m) + problem.c(k, i) + problem.c(j, l);
  v[2] += problem.c(h, k) + problem.c(m, i) + problem.c(j, l);
  v[3] += problem.c(h, m) + problem.c(k, j) + problem.c(i, l);

  return v;
}

void OPT3Move::apply(const TSPProblem &problem, TSPSolution &solution) const {
  if (to >= problem.n())
    throw std::runtime_error(
        "Index k out of bounds for this instance: k = " + std::to_string(to) +
        ", " + "N = " + std::to_string(problem.n()));

  vector<double> v = vars(problem, solution);
  double move_variation = variation(problem, solution);

  if (v[1] == move_variation) {
    // A more succinct way to manage this case is the following:
    // OPT2Move(from,to).apply(problem,solution);
    // OPT2Move(from,through).apply(problem,solution);
    // OPT2Move(thorugh+1,to).apply(problem,solution);

    // Anyway this is more inefficient:
    //      3n/2 + 3n/4 + 3n/4 = 3n assignments
    // where n is the length of of the sequence (from,...,to)

    // The way I chose to manage this case uses:
    //      n + n/2 = 3n/2 assignments

    vector<u_int> temp;
    if (through - from + 1 < to - through) {
      temp = vector<u_int>(solution.nodes.begin() + from,
                           solution.nodes.begin() + through + 1);

      u_int i = 0;
      for (; i < to - through; ++i)
        solution.nodes[from + i] = solution.nodes[through + 1 + i];

      for (; i < to - from + 1; ++i)
        solution.nodes[from + i] = temp[i - (to - through)];

    } else {
      temp = vector<u_int>(solution.nodes.begin() + through + 1,
                           solution.nodes.begin() + to + 1);

      for (int i = through - from; i >= 0; --i)
        solution.nodes[to - (through - from) + i] = solution.nodes[from + i];

      for (u_int i = 0; i < to - through; ++i)
        solution.nodes[from + i] = temp[i];
    }
    solution.cost += v[1];
    return;
  }

  if (v[0] == move_variation) {
    OPT2Move(from, through).apply(problem, solution);
    OPT2Move(through + 1, to).apply(problem, solution);
    return;
  }

  if (v[2] == move_variation) {
    OPT2Move(from, to).apply(problem, solution);
    OPT2Move(from + to - through, to).apply(problem, solution);
    return;
  }

  if (v[3] == move_variation) {
    OPT2Move(from, to).apply(problem, solution);
    OPT2Move(from, from + to - through - 1).apply(problem, solution);
  }
}

double OPT3Move::variation(const TSPProblem &problem,
                           const TSPSolution &solution) const {

  vector<double> v = vars(problem, solution);

  // If there are two contiguous arcs then returns the variation
  // corresponding to the case 2
  if (from == through || to == through + 1)
    return v[1];

  return std::min(std::min(v[0], v[1]), std::min(v[2], v[3]));
}
