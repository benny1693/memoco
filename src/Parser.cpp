/**
 * @file Parser.cpp
 * @author Benedetto Cosentino (benedetto.cosentino@studenti.unipd.it)
 * @brief Implemtations file for the Parser
 * @version 1
 * @date 2020-12-31
 *
 * @copyright Copyright (c) 2020
 *
 */

#include "../include/Parser.hpp"

TSPProblem Parser::readFile(const std::string &filename, bool print) const {

  std::cout << "Parsing \"" << filename << "\"... " << std::flush;
  std::ifstream file(filename);

  if (!file)
    throw std::runtime_error("File does not exist");

  std::string token;
  file >> token;

  while (!file.eof() && token != "BEGIN")
    file >> token;

  if (!file.eof()) {

    file >> token;

    vector<Position> positions;
    while (!file.eof() && token != "END") {
      std::stringstream tokenConverter;
      tokenConverter << token;

      Position p;

      tokenConverter >> p.first;
      file >> p.second;

      positions.push_back(p);
      file >> token;
    }

    if (token == "END") {
      if (print) {
        int it = 1;
        for (auto i : positions)
          std::cout << std::endl
                    << it++ << ":\t " << i.first << "\t" << i.second;
        std::cout << std::endl << "Parsing completed!" << std::endl;
      } else
        std::cout << "completed!" << std::endl;

      return TSPProblem(positions);
    } else
      throw std::runtime_error("Input file incorrect");
  } else
    throw std::runtime_error("Input file non existent or incorrect");
}
