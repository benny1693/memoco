/**
 * @file LocalSearch.hpp
 * @author Benedetto Cosentino (benedetto.cosentino@studenti.unipd.it)
 * @brief Implementation file for LocalSearch class
 * @version 1
 * @date 2021-01-05
 *
 * @copyright Copyright (c) 2021
 *
 */

#include "../include/LocalSearch.hpp"

TSPSolution LocalSearch::random_solution() const {

  TSPSolution solution = {vector<u_int>(problem.n()), 0.0};

  // Generate random sequence
  for (int i = 0; i < (int)problem.n(); ++i)
    solution.nodes[i] = i;
  std::random_shuffle(solution.nodes.begin(), solution.nodes.end());

  // Compute costs
  for (int i = 0; i < (int)problem.n() - 1; ++i)
    solution.cost += problem.c(solution.nodes[i], solution.nodes[(i + 1)]);

  solution.cost +=
      problem.c(solution.nodes[problem.n() - 1], solution.nodes[0]);

  return solution;
}

TSPSolution LocalSearch::random_solve() const {
  TSPSolution start_solution = random_solution();
  return solve(start_solution);
}

LocalSearch::LocalSearch(const TSPProblem &p) : problem(p) {}

TSPSolution LocalSearch::solve(const TSPSolution &start_solution) const {

  TSPSolution solution = {start_solution.nodes, start_solution.cost};

  while (auto move = next_move(solution))
    move->apply(problem, solution);

  return solution;
}

TSPSolution LocalSearch::random_multistart_solve(u_int n) const {

  srand(unsigned(std::time(0)));
  TSPSolution best;
  TSPSolution actual;
  best.cost = std::numeric_limits<double>::max();

  for (u_int i = 0; i < n; ++i) {
    actual = random_solve();
    if (actual.cost < best.cost)
      best = actual;
  }

  return best;
}
